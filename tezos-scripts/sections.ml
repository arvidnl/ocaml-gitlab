open Util

type format = CSV
type section_tbl = (string, int * int * int option) Hashtbl.t

let get_job_sections ~project_id ~token ~job_id () : section_tbl Gitlab.Monad.t
    =
  let open Gitlab in
  let open Monad in
  let* trace =
    let+ trace = Gitlab.Project.job_trace ~token ~project_id ~job_id () in
    Gitlab.Response.value trace
  in
  let split_on_chars chars s =
    List.fold_left
      (fun ss char -> List.concat_map (String.split_on_char char) ss)
      [ s ] chars
  in
  match trace with
  | None ->
      raise
      @@ Invalid_argument
           (sf "Couldn't retrieve the trace of job #%d in project %d" job_id
              project_id)
  | Some trace ->
      let sections : (string, int * int * int option) Hashtbl.t =
        Hashtbl.create 5
      in
      let re = Rex.compile "section_(end|start):(\\d+):(\\w+)" in
      let idx = ref 0 in
      List.iter
        (fun line ->
          match Rex.(line =~*** re) with
          | Some ("start", start_time, section_name) ->
              Hashtbl.replace sections section_name
                (!idx, int_of_string start_time, None);
              incr idx
          | Some ("end", end_time, section_name) -> (
              match Hashtbl.find_opt sections section_name with
              | Some (idx, start_time, _) ->
                  Hashtbl.replace sections section_name
                    (idx, start_time, Some (int_of_string end_time))
              | None -> ())
          | Some _ -> ()
          | None -> ())
        (split_on_chars [ '\n'; '\r' ] trace);
      return sections

let get_mr_jobs_sections ~project_id ~token ~merge_request_iid job_name () :
    (int * section_tbl) Gitlab.Stream.t Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let* pipelines =
    return
    @@ Gitlab.Project.merge_request_pipelines ~project_id ~token
         ~merge_request_iid ()
  in
  let jobs =
    Stream.map
      (fun (pipeline : Gitlab_t.pipeline) ->
        try
          let jobs =
            Gitlab.Project.pipeline_jobs ~per_page:200 ~project_id ~token
              ~pipeline_id:pipeline.id ()
          in
          let* jobs = Stream.to_list jobs in
          return
            (List.filter
               (fun (job : Gitlab_t.pipeline_job) -> job.name = job_name)
               jobs)
        with _ -> return [])
      pipelines
  in
  let job_sections =
    Stream.map
      (fun (job : Gitlab_t.pipeline_job) ->
        let* sections = get_job_sections ~project_id ~token ~job_id:job.id () in
        return [ (job.id, sections) ])
      jobs
  in
  return job_sections

let format_job_sections ~project_id format
    (job_sections : (int * section_tbl) list) =
  ignore format;
  let open Gitlab in
  let open Monad in
  let* () = eft "Sections in the trace of project %d:\n" project_id in

  match job_sections with
  | [] -> eft "No jobs found\n"
  | (_, fst_job_section) :: _ ->
      let headers =
        fst_job_section |> Hashtbl.to_seq |> List.of_seq
        |> List.sort (fun (_, (idx, _, _)) (_, (idx', _, _)) ->
               Int.compare idx idx')
        |> List.map (fun (name, (_, _, _)) -> name)
      in
      let* () = pft "Job,%s\n" (String.concat "," headers) in
      monad_list_iter job_sections @@ fun (job_id, sections) ->
      let values =
        List.map
          (fun section_name ->
            match Hashtbl.find_opt sections section_name with
            | Some (_idx, start_time, Some end_time) ->
                string_of_int (end_time - start_time)
            | Some (_idx, _start_time, None) -> "NaN"
            | None -> "-")
          headers
      in
      let* () = pft "#%d,%s\n" job_id (String.concat "," values) in
      return ()

let () =
  let project_id = ref None in
  let pipeline_id = ref None in
  let merge_request_iid = ref None in
  let job_name = ref None in
  let job_id = ref None in
  let token = ref None in
  let format = ref CSV in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        " <TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        " <PROJECT_ID> Numeric GitLab project id." );
      ( "--pipeline-id",
        Arg.Int (fun i -> pipeline_id := Some i),
        " <PROJECT_ID> Numeric GitLab pipeline id." );
      ( "--job-id",
        Arg.Int (fun i -> job_id := Some i),
        " <PROJECT_ID> Numeric GitLab job id." );
      ( "--mr-id",
        Arg.Int (fun i -> merge_request_iid := Some i),
        " <PROJECT_ID> Numeric merge request id." );
      ( "--job-name",
        Arg.String (fun s -> job_name := Some s),
        " <JOB_NAME> Numeric merge request id." );
      ( "--format",
        Arg.String
          (function
          | "csv" -> format := CSV
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown', 'json', 'csv' or \
                    'ascii'"),
        " Format: either 'markdown', 'md', 'json', 'csv' or 'ascii'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options] <pipeline-id>\nOptions are:"
      Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s ->
      raise
      @@ Invalid_argument
           (sf "Don't know what to do with extra arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let format = !format in
  let pipeline_id = !pipeline_id in
  let job_id = !job_id in
  let job_name = !job_name in
  let merge_request_iid = !merge_request_iid in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (match (merge_request_iid, pipeline_id, job_id) with
         | Some merge_request_iid, None, None -> (
             match job_name with
             | Some job_name ->
                 let* job_sections =
                   get_mr_jobs_sections ~project_id ~token ~merge_request_iid
                     job_name ()
                 in
                 let* job_sections = Gitlab.Stream.to_list job_sections in
                 format_job_sections ~project_id format job_sections
             | None ->
                 raise
                 @@ Invalid_argument
                      (sf "When --mr-id is given, --job-name must be passed."))
         | None, None, Some job_id ->
             let* sections = get_job_sections ~project_id ~token ~job_id () in
             format_job_sections ~project_id format [ (job_id, sections) ]
         | None, Some _pipeline_id, None -> assert false
         | _, _, _ ->
             raise
             @@ Invalid_argument
                  (sf "Must exactly one of --job-id, --mr-id or --pipeline-id.")))
