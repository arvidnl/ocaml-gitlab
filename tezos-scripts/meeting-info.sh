#!/bin/sh

set -eu

dune build ./testing_effort.exe
tool=""

private_token=$(keyring get 'gitlab-api' 'arvidnl/python-gitlab_api')

if [ -z "${1:-}" ]; then
    since=$(date --date="last thursday-7 days" --rfc-3339 seconds)
else
    since=$(date --date="$1" --rfc-3339 seconds)
fi

echo "Displaying MRs merged & active since $since"

echo "### Wins"
echo
echo "#### Merged MRs"
echo
../_build/default/tezos-scripts/testing_effort.exe --private-token "$private_token" --since "$since" --label "test ⚒" --format md --action merged
echo
echo "#### Open & active MRs"
echo
../_build/default/tezos-scripts/testing_effort.exe --private-token "$private_token" --since "$since" --label "test ⚒" --format md --action open-active
