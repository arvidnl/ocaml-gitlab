let partial_resolution = 8
let block = "█"

let partial_block parts =
  (* https://www.cogsci.ed.ac.uk/~richard/utf-8.cgi?input=%E2%96%88&mode=char *)
  if parts >= partial_resolution then
    raise (Invalid_argument "partial_bar: parts >= 8");
  let last = 136 + (partial_resolution - parts - 1) in
  [ 226; 150; last ] |> List.map Char.chr |> List.to_seq |> Bytes.of_seq
  |> Bytes.to_string

module Bar = struct
  type t = { label : string; length : int }

  let to_string { label; _ } = label
  let length { length; _ } = length
  let empty = { label = ""; length = 1 }

  let add b1 b2 =
    { label = b1.label ^ b2.label; length = b1.length + b2.length }

  let map_label b f = { b with label = f b.label }

  let repeat ch n =
    let rec aux b = function
      | 0 -> b
      | n -> aux (add b { label = ch; length = 1 }) (n - 1)
    in
    aux empty n
end

let partial_bar (resolution : int) (length : int) (color_opt : Color.t option) =
  if length = 0 then Bar.empty
  else if length > resolution then
    raise (Invalid_argument "partial_bar: length > resolution")
  else
    let parts = partial_resolution * length / resolution in
    let pbar = partial_block parts in
    { label = Color.apply_opt color_opt pbar; length = 1 }

let bar (start : int) (resolution : int) (len : int)
    (color_opt : Color.t option) : Bar.t =
  let bar = Bar.empty in
  let bar = Bar.(add bar (repeat " " @@ (start / resolution))) in
  let bar =
    if start + len < start - (start mod resolution) + resolution then
      (* the bar does not fill a full block *)
      if start mod resolution < resolution / 2 then
        Bar.(add bar (partial_bar resolution len color_opt))
      else
        let inverse =
          (* export let inverse: code = { open: '\u001b[7m', close: '\u001b[27m' }; *)
          assert false
        in
        Bar.(
          add bar
            (map_label
               (partial_bar resolution (resolution - len) color_opt)
               inverse))
    else assert false
  in
  bar

let () =
  for i = 0 to partial_resolution + 1 do
    Printf.printf "partial_block(%d): %s\n" i (partial_block i)
  done
