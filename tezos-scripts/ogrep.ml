let read_line_opt () = try Some (read_line ()) with End_of_file -> None

let () =
  let open Rex in
  let matched = ref false in
  match Sys.argv with
  | [| _; re |] ->
      let re = compile re in
      let rec aux () =
        match read_line_opt () with
        | Some line when line =~ re ->
            matched := true;
            Format.printf "%s\n" line;
            aux ()
        | Some _ ->
            (*             Format.printf "> Read line: %s\n" line; *)
            aux ()
        | None ->
            (*             Format.printf "< End\n"; *)
            ()
      in
      aux ();
      exit (if !matched then 0 else 1)
  | _ ->
      Format.eprintf "Usage: ./ogrep.exe <regexp>";
      exit 2
