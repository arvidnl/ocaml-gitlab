#!/bin/bash

#shellcheck disable=SC2002

# TODO: use https://www.sqlitetutorial.net/sqlite-group_concat/

# TODO: the report should say which is the oldest and which is the newest of hte pipelins (their dates)

usage() {
    echo "Usage: $0 [<retried_tests.log>]"
    echo ""
    echo "<retried_tests.log> defaults to retried_tests_<TODAYS-DATE>.log"
}

default_retried_tests_log=retried_tests_"$(date +%Y-%m-%d)".log
if [ $# = 0 ]; then
    retried_tests_log="$default_retried_tests_log"
elif [ $1 == "--help" ]; then
    usage
    exit 0
else
    retried_tests_log="$1"
fi

if [ ! -f "$retried_tests_log" ]; then
    echo "$retried_tests_log" does not exist
    usage
    exit 1
fi

echo "# Overview"
echo ""

grep "^Restricting" "$retried_tests_log"
grep "^Analyzed" "$retried_tests_log"
echo ""

cat "$retried_tests_log" | grep 'Count,' -A 5000 | sed 's@tests_[^/]*/\(.*\)::[^:]\+,@\1,@' | csvsql --query 'select sum(Count) as `#`, `Extra info`, "`" || `Job name` || "` " || `Example` as `Ex. job` from stdin group by `Extra info`' | csvsort -c 1 -r |  sed 's@\(https[^ ]*\/\([[:digit:]]\+\)\)@\[#\](\1)@' | csvlook

echo ""
echo "# Unclassified"
echo ""
cat "$retried_tests_log" | grep 'Count,' -A 5000 | sed 's@tests_[^/]*/\(.*\)::[^:]\+,@\1,@' | csvsql --query 'select Count as `#`, "`" || `Job name` || "` " as `Job name`, `Example` as `Ex. job` from stdin where `Extra info` is NULL' | csvsort -c 1 -r | sed 's@\(https[^ ]*\/\([[:digit:]]\+\)\)@\[#\](\1)@' | csvlook

echo
echo "/label ~\"test ⚒\" ~\"type::incident\" ~\"CI ⚙\""
