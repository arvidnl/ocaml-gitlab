#!/bin/bash

outfile=retried_tests_"$(date +%Y-%m-%d)"
opts=""
if [ -n "$1" ]; then
    rebased_sha="$1"
    opts="$opts --rebased-on $1"
    outfile="$outfile--$rebased_sha"
fi
outfile="$outfile.log"

echo "Output in $outfile"
PRIVATE_TOKEN=${PRIVATE_TOKEN:-"$(keyring get 'gitlab-api' 'arvidnl/python-gitlab_api')"} dune exec ./retried_tests.exe -- --limit 500 --failure-reason script_failure $opts | tee "$outfile"
