open Util

(*

for all pipelines on project X on branch Y,
  get all failling jobs
   for each job, get the trace
     in the trace, find the failing jobs

*)

type exn += Invalid_date of string

let per_page = 200
let string_of_datetime = ISO8601.Permissive.string_of_datetime
let show_pipeline_job_status = Gitlab_j.string_of_pipeline_job_status

(* a job (identified by its name) is 'retried' if it appears several times.
   it is flaky if it appears once with  'success'  and once with 'failed'
*)
(* should return [(job_name, [successes], [job_failure])] *)
type job_failure = {
  job : Gitlab_t.pipeline_job;
  reason : Tezos_gitlab.extra_info option;
}

let hashtbl_update tbl ~default k f =
  f (Option.value ~default @@ Hashtbl.find_opt tbl k) |> Hashtbl.replace tbl k

let get_failed_pipeline ~token ~project_id ~pipeline_id () :
    job_failure list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let* failed_jobs =
    return
    @@ Gitlab.Project.pipeline_jobs ~token ~project_id ~per_page ~pipeline_id
         ~scope:`Failed ~include_retried:true ()
  in
  let* failed_jobs = Stream.to_list failed_jobs in
  monad_list_map_p failed_jobs @@ fun job ->
  let+ reason = Tezos_gitlab.get_extra_info ~token ~project_id job in
  { job; reason }

module StringSet = Set.Make (struct
  type t = string

  let compare = String.compare
end)

type pipeline_failure = {
  pipeline : Gitlab_t.pipeline;
  failures : job_failure list;
}

let get_failed ?sha ?ref_ ~token project_id :
    pipeline_failure list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  (*   let* updated_after = *)
  (*     match rebased_on with *)
  (*     | Some sha -> *)
  (*         let* ref = Gitlab.Project.Commit.ref ~token ~project_id ~sha () in *)
  (*         let ref = Gitlab.Response.value ref in *)
  (*         let created_at = string_of_datetime ref.commit_created_at in *)
  (*         let+ () = *)
  (*           pft "Restricting to pipelines rebased on %s (updated_after: %s)\n" sha *)
  (*             created_at *)
  (*         in *)
  (*         Some created_at *)
  (*     | None -> return None *)
  (*   in *)
  let* pipelines =
    return
    @@ Gitlab.Project.pipelines ?sha ?ref_ ~token ~project_id ~per_page
         ~status:`Failed ()
  in

  let* pipelines =
    (* stream_take 5 *)
    (*     stream_take 20 pipelines *)
    Stream.to_list pipelines
  in
  let len_pipelines = List.length pipelines in
  monad_list_concat_mapi_p pipelines @@ fun i pipeline ->
  let* () =
    pft "%d/%d: Pipeline #%d created at %s:\n" i len_pipelines pipeline.id
      (string_of_datetime pipeline.created_at)
  in
  let* failures =
    get_failed_pipeline ~token ~project_id ~pipeline_id:pipeline.id ()
  in
  return [ { pipeline; failures } ]

type format = Ascii | Markdown | JSON | CSV

let format_failure_of_pipeline_ascii ~pipeline_id { job; reason } =
  pf "In pipeline #%d, job %d (%s): %a\n" pipeline_id job.id job.name
    Tezos_gitlab.pp_extra_info_opt reason

let format_failure_of_pipeline_csv ~pipeline_id { job; reason } =
  pf "#%d,%d,%s,%a\n" pipeline_id job.id job.name Tezos_gitlab.pp_extra_info_opt
    reason

let format_failures ?ref_ ~project_id format
    (pipeline_failures : pipeline_failure list) =
  let open Gitlab_t in
  match format with
  | CSV ->
      pf "pipeline_id,job_id,job_name,reason\n";
      List.iter
        (fun { pipeline; failures } ->
          List.iter
            (format_failure_of_pipeline_csv ~pipeline_id:pipeline.id)
            failures)
        pipeline_failures
  | Ascii | _ ->
      pf
        "Found %d failed pipelines with a total of %d failed jobs for ref %s \
         in project %d:\n\n"
        (List.length pipeline_failures)
        (List.fold_left
           (fun total { failures; _ } -> total + List.length failures)
           0 pipeline_failures)
        (Option.value ~default:"*" ref_)
        project_id;
      List.iter
        (fun { pipeline; failures } ->
          List.iter
            (format_failure_of_pipeline_ascii ~pipeline_id:pipeline.id)
            failures)
        pipeline_failures

let format_failures_of_pipeline ~project_id ~pipeline_id format
    (failures : job_failure list) =
  match format with
  | CSV ->
      pf "pipeline_id,job_id,job_name,reason\n";
      List.iter (format_failure_of_pipeline_csv ~pipeline_id) failures
  | Ascii | _ ->
      pf "Found %d failures in pipeline %d in project %d:\n\n"
        (List.length failures) pipeline_id project_id;
      List.iter (format_failure_of_pipeline_ascii ~pipeline_id) failures

(* let open Gitlab_t in
 * pf "%d MRs open & active since %a with labels %s:\n\n" (List.length mrs)
 *   (Ptime.pp_rfc3339 ()) since
 *   (String.concat ", " labels);
 * List.iter
 *   (fun mr ->
 *     match format with
 *     | Ascii ->
 *         pf "#%d [updated: %s]: %s\n" mr.merge_request_iid
 *           (Gitlab_json.DateTime.unwrap mr.merge_request_updated_at)
 *           mr.merge_request_title
 *     | Markdown ->
 *         pf " - [!%d](%s): `%s`\n" mr.merge_request_iid
 *           mr.merge_request_web_url
 *           (String.concat "'"
 *              (String.split_on_char '`' mr.merge_request_title)))
 *   mrs *)

(* let format_retried ?rebased_on ?failure_reason ~project_id ~limit format *)
(*     (pipelines : retried_results list) = *)
(*   ignore limit; *)
(*   ignore rebased_on; *)
(*   ignore failure_reason; *)
(*   ignore project_id; *)
(*   ignore format; *)
(*   ignore pipelines; *)
(*   (\* group tests by reason *\) *)
(*   let flakes = *)
(*     List.filter *)
(*       (function { flakes = _ :: _; _ } -> true | _ -> false) *)
(*       pipelines *)
(*   in *)
(*   Format.printf *)
(*     "Analyzed %d pipelines in project %d, found %d flaky pipelines (%.2f%%)\n" *)
(*     (List.length pipelines) project_id (List.length flakes) *)
(*     (100.0 *)
(*     *. float_of_int (List.length flakes) *)
(*     /. float_of_int (List.length pipelines)); *)
(*   (\* group job_failure by job_name + extra info *\) *)
(*   let by_job_reason = Hashtbl.create 5 in *)
(*   List.iter *)
(*     (fun { flakes; _ } -> *)
(*       List.iter *)
(*         (fun { name; job_failure; _ } -> *)
(*           List.iter *)
(*             (fun (job, extra_info) -> *)
(*               hashtbl_update by_job_reason ~default:[] (name, extra_info) *)
(*                 (fun jobs -> job :: jobs)) *)
(*             job_failure) *)
(*         flakes) *)
(*     flakes; *)
(*   let sorted = *)
(*     List.sort *)
(*       (fun (_, flakes1) (_, flakes2) -> *)
(*         Int.compare (List.length flakes2) (List.length flakes1)) *)
(*       (List.of_seq (Hashtbl.to_seq by_job_reason)) *)
(*   in *)
(*   Format.printf "Count,Job name,Extra info,Example\n"; *)
(*   List.iter *)
(*     (fun ((job_name, extra_info), job_failure) -> *)
(*       Format.printf "%d,%s,%s,%s\n" (List.length job_failure) job_name *)
(*         (match extra_info with No_extra_info -> "?" | Extra_info s -> s) *)
(*         (match job_failure with *)
(*         | job_failure :: _ -> job_failure.Gitlab_t.web_url *)
(*         | [] -> "!")) *)
(*     sorted *)

let () =
  (*   let limit = ref 20 in *)
  (* tezos/tezos *)
  let project_id = ref None in
  let pipeline_id = ref None in
  let token = ref None in
  let ref_ = ref None in
  let sha = ref None in
  let format = ref Markdown in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--pipeline-id",
        Arg.Int (fun i -> pipeline_id := Some i),
        "<ID> Look for flaky jobs in this pipeline only. Useful for debugging."
      );
      ( "--ref",
        Arg.String (fun s -> ref_ := Some s),
        "<ref> Only include pipelines for the given ref. " );
      ( "--sha",
        Arg.String (fun s -> sha := Some s),
        "<sha> Only include pipelines for the given sha. " );
      ( "--format",
        Arg.String
          (function
          | "md" | "markdown" -> format := Markdown
          | "ascii" -> format := Ascii
          | "json" -> format := JSON
          | "csv" -> format := CSV
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown', 'json', 'csv' or \
                    'ascii'"),
        " Format. Can be either 'markdown', 'md', 'json', 'csv' or 'ascii'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let ref_ = !ref_ in
  let sha = !sha in
  let format = !format in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (match !pipeline_id with
         | None ->
             get_failed ?sha ?ref_ ~token project_id
             >|= format_failures ?ref_ ~project_id format
         | Some pipeline_id ->
             get_failed_pipeline ~project_id ~token ~pipeline_id ()
             >|= format_failures_of_pipeline ~project_id ~pipeline_id format))
