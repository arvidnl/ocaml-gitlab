open Util

(*

MR states:

 - Creation)
 - Assigned to first non-author
 - "Final decision": Merged / closed (for simplicity, ignore MRs that are merged after an close event)




Per MR :

 - Time spent waiting for reviewer to be assigned then time spent MR is assigned to author / reviewer
   - Can be measured on time spent being assigned to author / non-author
   - Do we have notes API in the ocaml-gitlab? yes

 - Line changes

 - Reviewers

*)

(* first version:

   each row:

     mr id, state, title, lines changed, time from created to merged/closed
*)

type format = CSV

type mr_metrics = {
  mr : Gitlab_t.merge_request;
  numstat : Git.numstat;
      (*     ; *)
      (*     merge_time : int; *)
}

type ('a, 'b) col = {
  title : string;
  description : string;
  of_metrics : 'b -> 'a;
  format : format -> 'a -> string;
}

let format_string_csv s =
  let quote = ref false in
  let sanitized =
    String.map
      (function
        | '"' -> '\''
        | ',' as c ->
            quote := true;
            c
        | c -> c)
      s
  in
  if !quote then "\"" ^ sanitized ^ "\"" else sanitized

let col_iid =
  {
    title = "MR iid";
    description = "";
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_iid);
    format = (function CSV -> fun s -> sf "!%d" s);
  }

let col_title =
  {
    title = "MR title";
    description = "";
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_title);
    format = (function CSV -> fun s -> format_string_csv s);
  }

let col_state =
  {
    title = "MR state";
    description = "";
    of_metrics = (fun metrics -> metrics.mr.Gitlab_t.merge_request_state);
    format =
      (function
      | CSV -> fun s -> format_string_csv (Gitlab_j.string_of_state s));
  }

let col_additions =
  {
    title = "MR additions";
    description = "";
    of_metrics = (fun metrics -> metrics.numstat.additions);
    format = (function CSV -> fun i -> string_of_int i);
  }

let col_deletions =
  {
    title = "MR deletions";
    description = "";
    of_metrics = (fun metrics -> metrics.numstat.deletions);
    format = (function CSV -> fun d -> string_of_int d);
  }

let col_changes =
  {
    title = "MR changes";
    description = "";
    of_metrics = (fun metrics -> metrics.numstat);
    format =
      (function
      | CSV ->
          fun { additions; deletions } ->
            string_of_int @@ (additions + deletions));
  }

let cols_merge_time =
  let merge_time_s metrics =
    let merged_at =
      option_get_msg
        ~error_msg:
          (sf "Missing merged_at field in MR !%d" metrics.mr.merge_request_iid)
        metrics.mr.merge_request_merged_at
    in
    merged_at -. metrics.mr.merge_request_created_at
  in
  [
    {
      title = "MR merge time (seconds)";
      description = "Time between creation date and merge date in seconds";
      of_metrics = merge_time_s;
      format = (function CSV -> string_of_float);
    };
    {
      title = "MR merge time (days)";
      description = "Time between creation date and merge date in days";
      of_metrics =
        (fun metrics -> merge_time_s metrics /. (24.0 *. 60.0 *. 60.0));
      format = (function CSV -> string_of_float);
    };
  ]

type 'b ex_col = Ex_col : ('a, 'b) col -> 'b ex_col

let cols =
  [
    Ex_col col_iid;
    Ex_col col_title;
    Ex_col col_state;
    Ex_col col_additions;
    Ex_col col_deletions;
    Ex_col col_changes;
  ]
  @ List.map (fun c -> Ex_col c) cols_merge_time

let per_page = 200

let get_mr_metrics ?created_after ?created_before ?order_by ?sort ?limit ~token
    ~repository project_id : mr_metrics Gitlab.Stream.t =
  let open Gitlab in
  let open Monad in
  let map xs f = Stream.map f xs in
  let mrs =
    Gitlab.Project.merge_requests ~token ?created_after ?created_before
      ?order_by ?sort ~state:`Merged ~id:project_id ()
  in
  let mrs = Stream.(Option.fold ~none:Fun.id ~some:take limit mrs) in
  map mrs @@ fun mr ->
  match mr.merge_request_merge_commit_sha with
  | Some merge_commit_sha ->
      let parents = Git.commit_parents ~repository ~commit:merge_commit_sha in
      let merge_predecessor =
        let not_merge_predecessor =
          Option.value ~default:mr.merge_request_sha
            mr.merge_request_squash_commit_sha
        in
        match parents with
        | [ commit1; commit2 ] when commit1 = not_merge_predecessor -> commit2
        | [ commit1; commit2 ] when commit2 = not_merge_predecessor -> commit1
        | _ ->
            failwith
              (sf
                 "Could not figure out which of MR %d's parent commits (%a) is \
                  the merge predecessor. sha = %s, merge_commit_sha = %s (see \
                  %s)\n"
                 mr.merge_request_iid
                 Format.(pp_print_list pp_print_string)
                 parents mr.merge_request_sha merge_commit_sha
                 mr.merge_request_web_url)
      in
      let numstat =
        Git.numstat ~repository ~commit1:merge_predecessor
          ~commit2:merge_commit_sha
      in
      return [ { mr; numstat } ]
  | None ->
      let* () =
        eft ">> Ignoring MR !%d with no merge_commit_sha (see %s)\n"
          mr.merge_request_iid mr.merge_request_web_url
      in
      return []

let format_mr_metrics ?created_after ?created_before ?order_by ?sort ?limit
    ~token ~repository project_id format mrs =
  ignore repository;
  ignore created_after;
  ignore created_before;
  ignore order_by;
  ignore sort;
  ignore token;
  ignore limit;
  ignore project_id;
  ignore format;
  let open Gitlab in
  let open Monad in
  let filters =
    let show_order = function
      | `Created_at -> "updated_at"
      | `Title -> "user_id"
      | `Updated_at -> "updated_at"
    in
    (match limit with None -> [] | Some limit -> [ sf "limit=%d" limit ])
    @ (match created_after with
      | None -> []
      | Some created_after -> [ sf "created_after=%s" created_after ])
    @ (match created_before with
      | None -> []
      | Some created_before -> [ sf "created_before=%s" created_before ])
    @ (match order_by with
      | None -> []
      | Some order_by -> [ sf "order_by=%s" (show_order order_by) ])
    @
    match sort with
    | None -> []
    | Some sort -> [ sf "sort=%s" (Gitlab_j.string_of_sort sort) ]
  in
  let* mrs = Stream.to_list mrs in
  let* () =
    eft "MR metrics for merge requests in project %d, got %d MRs %s\n"
      project_id (List.length mrs)
      (match filters with
      | [] -> ""
      | _ -> sf " [%s]" (String.concat ", " filters))
  in
  let print_row_raw ~separator row = pft "%s\n" (String.concat separator row) in
  let print_row ~format ~cols ~separator content =
    let col_content =
      List.map
        (function Ex_col col -> col.format format @@ col.of_metrics content)
        cols
    in
    print_row_raw ~separator col_content
  in
  match format with
  | CSV ->
      let separator = "," in
      let* () =
        print_row_raw ~separator
        @@ List.map (function Ex_col c -> c.title) cols
      in
      (*       Stream.iter (fun mr -> print_row ~format:CSV ~cols ~separator mr) mrs *)
      monad_list_iter mrs (fun mr -> print_row ~format:CSV ~cols ~separator mr)

let () =
  let limit = ref None in
  (* tezos/tezos *)
  let project_id = ref None in
  let local_repo_path = ref None in
  let token = ref None in
  let created_after = ref None in
  let created_before = ref None in
  let order_by = ref None in
  let sort = ref None in
  let format = ref CSV in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--local-repo",
        Arg.String (fun s -> local_repo_path := Some s),
        "<TOKEN> GitLab private token." );
      ( "--created-after",
        Arg.String (fun date -> created_after := Some date),
        "Only include merge requests created after" );
      ( "--created-before",
        Arg.String (fun date -> created_before := Some date),
        "Only include merge requests created before" );
      ( "--order-by",
        Arg.String
          (fun order ->
            let order =
              match order with
              | "created_at" -> `Created_at
              | "title" -> `Title
              | "updated_at" -> `Updated_at
              | _ -> assert false
            in
            order_by := Some order),
        "Order results by" );
      ( "--sort",
        Arg.String
          (fun sort_dir ->
            let sort_dir =
              match sort_dir with
              | "asc" -> `Asc
              | "desc" -> `Desc
              | _ -> assert false
            in
            sort := Some sort_dir),
        "Order results by" );
      ( "--limit",
        Arg.Int
          (fun l ->
            if l < 0 then raise (Invalid_argument "--limit must be positive")
            else limit := Some l),
        sf
          "<LIMIT> Only search up to this merge requests pipelines (defaults \
           to no limit)" );
      ( "--format",
        Arg.String
          (function
          (*           | "md" | "markdown" -> format := Markdown *)
          (*           | "ascii" -> format := Ascii *)
          (*           | "json" -> format := JSON *)
          | "csv" -> format := CSV
          | _ -> raise @@ Invalid_argument "--format must be either 'csv'"),
        " Format. Can be either 'csv'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let limit = !limit in
  let format = !format in
  let created_after = !created_after in
  let created_before = !created_before in
  let order_by = !order_by in
  let sort = !sort in
  let local_repo_path =
    match !local_repo_path with
    | Some local_repo_path -> local_repo_path
    | None -> failwith "Please pass --local-repo"
  in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (let mrs =
            get_mr_metrics ?created_after ?created_before ?order_by ?sort ?limit
              ~token ~repository:local_repo_path project_id
          in
          format_mr_metrics ?created_after ?created_before ?order_by ?sort
            ?limit ~token ~repository:local_repo_path project_id format mrs))
