module StringSet = Set.Make (struct
  type t = string

  let compare = String.compare
end)

let sf = Format.asprintf
let ff = Format.fprintf
let pf = Format.printf

let pft fmt =
  let open Gitlab in
  let print s = Monad.embed (Lwt_io.print s) in
  Printf.ksprintf print fmt

let eft fmt =
  let open Gitlab in
  let print s = Monad.embed (Lwt_io.eprint s) in
  Printf.ksprintf print fmt

let rec stream_limited_find :
    limit:int ->
    ('a -> bool) ->
    'a Gitlab.Stream.t ->
    ('a * 'a Gitlab.Stream.t) option Gitlab.Monad.t =
 fun ~limit f s ->
  let open Gitlab.Monad in
  if limit <= 0 then return None
  else
    let* head_opt = Gitlab.Stream.next s in
    match head_opt with
    | Some (v, _) when f v -> return head_opt
    | Some (_, s') -> stream_limited_find ~limit:(limit - 1) f s'
    | None -> return None

let option_get_msg ~error_msg = function
  | Some v -> v
  | None -> failwith ("[option_get_msg]: " ^ error_msg)

let rec stream_take : int -> 'a Gitlab.Stream.t -> 'a list Gitlab.Monad.t =
 fun n s ->
  let open Gitlab.Monad in
  if n <= 0 then return []
  else
    let* head_opt = Gitlab.Stream.next s in
    match head_opt with
    | Some (v, s) ->
        let+ tail = stream_take (n - 1) s in
        v :: tail
    | None -> return []

let rec stream_take_while_map :
    ('a -> 'b option) -> 'a Gitlab.Stream.t -> 'b list Gitlab.Monad.t =
 fun f s ->
  let open Gitlab.Monad in
  let* head_opt = Gitlab.Stream.next s in
  match head_opt with
  | Some (v, s) -> (
      match f v with
      | Some v' ->
          let+ tail = stream_take_while_map f s in
          v' :: tail
      | None -> return [])
  | None -> return []

let parse_config ~default_project_id ~default_token =
  let config_file = "gl-cfg.json" in
  let token = ref default_token in
  let project_id = ref default_project_id in
  if Sys.file_exists config_file then (
    let config_json = Yojson.Basic.from_file config_file in
    if Option.is_some (Sys.getenv_opt "PRIVATE_TOKEN") && Option.is_none !token
    then token := Some (Sys.getenv "PRIVATE_TOKEN" |> Gitlab.Token.of_string);
    if
      Option.is_none !token
      && List.mem "gitlab-token" (Yojson.Basic.Util.keys config_json)
    then
      token :=
        Some
          Yojson.Basic.Util.(
            config_json |> member "gitlab-token" |> to_string
            |> Gitlab.Token.of_string);
    if
      Option.is_none !project_id
      && List.mem "project-id" (Yojson.Basic.Util.keys config_json)
    then
      project_id :=
        Some Yojson.Basic.Util.(config_json |> member "project-id" |> to_int));
  ( (match !token with
    | None ->
        raise
          (Invalid_argument
             (sf
                "A private token must be provided either through \
                 '--private-token' or in %s"
                config_file))
    | Some t -> t),
    match !project_id with
    | None ->
        raise
          (Invalid_argument
             (sf
                "A project id must be provided either through '--project_id' \
                 or in %s"
                config_file))
    | Some i -> i )

let rec monad_list_iter xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* () = f x in
      monad_list_iter xs f
  | [] -> return ()

let rec monad_list_map_s xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x in
      let* xs' = monad_list_map_s xs f in
      return (x' :: xs')
  | [] -> return []

let rec monad_list_map_p xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x and* xs' = monad_list_map_p xs f in
      return (x' :: xs')
  | [] -> return []

let rec monad_list_concat_map_s xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x in
      let* xs' = monad_list_concat_map_s xs f in
      return (x' @ xs')
  | [] -> return []

let rec monad_list_concat_map_p xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x and* xs' = monad_list_concat_map_p xs f in
      return (x' @ xs')
  | [] -> return []

let monad_list_concat_mapi_p xs f =
  monad_list_concat_map_p
    (List.mapi (fun i x -> (i, x)) xs)
    (fun (i, x) -> f i x)

let rec take k xs =
  match k with
  | 0 -> []
  | k -> ( match xs with [] -> [] | x :: ys -> x :: take (k - 1) ys)

(* let rec monad_list_concat_map_chunked ~chunk_size xs f = *)
(*   let open Gitlab.Monad in *)

(*   let rec take k xs = match k with *)
(*     | 0 -> ([], xs) *)
(*     | k -> *)
(*        match xs with *)
(*        | [] -> failwith "take" *)
(*        | x :: xs' -> *)
(*           let (hds, tl) = (take (k - 1) ys) in *)
(*           (x :: hds, tl) *)
(*   in *)
(*   let chunks n xs = *)
(*     take n xs :: *)

(*   match xs with *)
(*   | x :: xs -> *)
(*       let* x' = f x in *)
(*       let* xs' = monad_list_concat_map_s xs f in *)
(*       return (x' @ xs') *)
(*   | [] -> return [] *)
