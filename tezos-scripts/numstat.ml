let () =
  match Sys.argv with
  | [| _; repository; commit1; commit2 |] ->
      let Git.{ additions; deletions } =
        Git.numstat ~repository ~commit1 ~commit2
      in
      Format.printf "+%d-%d\n" additions deletions
  | _ ->
      Format.printf "Usage: %s <repo-path> <commit1> <commit2>\n" Sys.argv.(0)
