open Util

type exn += Invalid_date of string
type action = Merged | Open_active

let action_to_string = function
  | Merged -> "merged"
  | Open_active -> "open-active"

type format = Ascii | Markdown

let get_open_active ~token ~project_id labels since =
  let open Gitlab in
  let open Monad in
  let* mrs =
    return
    @@ Gitlab.Project.merge_requests ~token ~labels ~state:`Opened
         ~id:project_id
         ~updated_after:(ISO8601.Permissive.string_of_datetime since)
         ()
  in
  Stream.to_list mrs

let format_open_active labels since format mrs =
  let open Gitlab_t in
  pf "%d MRs open & active since %a with labels %s:\n\n" (List.length mrs)
    ISO8601.Permissive.pp_datetime since
    (String.concat ", " labels);
  List.iter
    (fun mr ->
      match format with
      | Ascii ->
          pf "#%d [updated: %s]: %s\n" mr.merge_request_iid
            (Gitlab_json.DateTime.unwrap mr.merge_request_updated_at)
            mr.merge_request_title
      | Markdown ->
          pf " - [!%d](%s): `%s`\n" mr.merge_request_iid
            mr.merge_request_web_url
            (String.concat "'"
               (String.split_on_char '`' mr.merge_request_title)))
    mrs

let get_merged ~token ~project_id labels since =
  let open Gitlab in
  let open Monad in
  let* mrs =
    return
    @@ Gitlab.Project.merge_requests ~token ~labels ~state:`Merged
         ~id:project_id
         ~updated_after:(ISO8601.Permissive.string_of_datetime since)
         ()
  in
  stream_take_while_map
    (fun mr ->
      match mr.Gitlab_t.merge_request_merged_at with
      | Some merged_at ->
          if since <= merged_at then Some (mr, merged_at)
          else None
            (*           match Ptime.of_rfc3339 merged_at_string with *)
            (*           | Ok (merged_at, _tz, _count) -> *)
            (*               if Ptime.compare since merged_at <= 0 then Some (mr, merged_at) *)
            (*               else None *)
            (*           | Error (`RFC3339 (_, e)) -> *)
            (*               raise *)
            (*               @@ Invalid_date *)
            (*                    (sf "Invalid RFC3339 date in MR !%d: %a" *)
            (*                       mr.Gitlab_t.merge_request_iid Ptime.pp_rfc3339_error e) *)
      | None -> None)
    mrs

let format_merged labels since format merged_mrs =
  let open Gitlab_t in
  pf "%d MRs merged since %a with labels %s:\n\n" (List.length merged_mrs)
    ISO8601.Permissive.pp_datetime since
    (String.concat ", " labels);
  List.iter
    (fun (mr, merged_at) ->
      match format with
      | Ascii ->
          pf "#%d [merged: %a]: %s\n" mr.merge_request_iid
            ISO8601.Permissive.pp_datetime merged_at mr.merge_request_title
      | Markdown ->
          pf " - [!%d](%s): `%s`\n" mr.merge_request_iid
            mr.merge_request_web_url
            (String.concat "'"
               (String.split_on_char '`' mr.merge_request_title)))
    merged_mrs

let () =
  let action = ref Merged in
  let labels = ref [] in
  let since = ref 0.0 in
  let format = ref Ascii in
  let project_id = ref None in
  (* tezos/tezos *)
  let token = ref None in
  let args_spec =
    [
      ( "--action",
        Arg.String
          (function
          | "merged" -> action := Merged
          | "open-active" -> action := Open_active
          | _ ->
              raise
              @@ Invalid_argument
                   "--action must be either 'merged' or 'open-active'"),
        " Action. Can be either 'merged' (Show merged MRs, default) or\n\
        \        'open-active' (show open and active MRs)." );
      ( "--format",
        Arg.String
          (function
          | "md" | "markdown" -> format := Markdown
          | "ascii" -> format := Ascii
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown' or 'ascii'"),
        " Format. Can be either 'markdown', 'md'or 'ascii'." );
      ( "--label",
        Arg.String (fun label -> labels := label :: !labels),
        " Show only resources with this label. can be supplied multiple times \
         (intersection)" );
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        " GitLab project id." );
      ( "--project-id",
        Arg.Int (fun project_id_s -> project_id := Some project_id_s),
        " GitLab private token." );
      ( "--since",
        Arg.String
          (fun date_str -> since := ISO8601.Permissive.datetime date_str),
        (*             | Error (`RFC3339 (_, e)) -> *)
        (*                 raise *)
        (*                 @@ Invalid_argument *)
        (*                      (sf "--since must be a valid RFC3339 date: %a" *)
        (*                         Ptime.pp_rfc3339_error e) ), *)
        " (Required) show only resources whose state change date superior than \
         this value on the format Y-m-d" );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (match !action with
         | Merged ->
             get_merged ~project_id ~token !labels !since
             >|= format_merged !labels !since !format
         | Open_active ->
             get_open_active ~project_id ~token !labels !since
             >|= format_open_active !labels !since !format))
