type failure = { description : string; retry_command : string option }

type extra_info =
  | Extra_info of {
      failures : failure list;
      full_retry_command : string option;
    }

let pp_extra_info fmt = function
  | Extra_info { failures; full_retry_command = _ } ->
      Format.fprintf fmt "%s"
        (String.concat ";"
        @@ List.map
             (fun { description; retry_command = _ } -> description)
             failures)

let show_extra_info extra_info = Format.asprintf "%a" pp_extra_info extra_info

let pp_extra_info_opt fmt extra_info_opt =
  Format.fprintf fmt "%s"
    (Option.fold ~none:"?" ~some:show_extra_info extra_info_opt)

let show_extra_info_opt extra_info_opt =
  Format.asprintf "%a" pp_extra_info_opt extra_info_opt

let get_extra_info ~token ~project_id (job : Gitlab_t.pipeline_job) :
    extra_info option Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let grep re s =
    let open Rex in
    let re = compile re in
    s |> String.split_on_char '\n'
    |> List.filter_map (fun s ->
           Option.map
             (fun matches ->
               matches |> Array.to_list |> List.tl |> String.concat "")
             (s =~^ re))
  in
  let get_trace_opt =
    let* trace =
      Gitlab.Project.job_trace ~token ~project_id ~job_id:job.id ()
    in
    let trace = Gitlab.Response.value trace in
    return trace
  in
  let grep_trace res =
    let+ trace_opt = get_trace_opt in
    match trace_opt with
    | Some trace ->
        Some
          (List.concat_map
             (fun (re, find_retry_command) ->
               List.map
                 (fun matched_string ->
                   {
                     description = matched_string;
                     retry_command = find_retry_command matched_string;
                   })
                 (grep re trace))
             res)
    | None -> None
  in
  let split_on_chars chars s =
    List.fold_left
      (fun ss char -> List.concat_map (String.split_on_char char) ss)
      [ s ] chars
  in
  let general_patterns =
    [
      ("(Corrupted coverage files were found)", Fun.const None);
      (*       ERROR: Job failed: failed to pull image "[MASKED]/gitlab-runner-helper:x86_64-v14.10.1" with specified policies [if-not-present]: unexpected EOF (manager.go:203:1s) *)
      ( "(ERROR: Job failed: failed to pull image) \".*(: unexpected EOF)",
        Fun.const None );
      (*       ERROR: Job failed: failed to pull image "[MASKED]/gitlab-runner-helper:x86_64-v14.10.1" with specified policies [if-not-present]: error during connect: Post "https://10.0.132.38:2376/v1.41/images/create?fromImage=[MASKED]%2Fgitlab-runner-helper&tag=x86_64-v14.10.1": EOF (manager.go:203:0s) *)
      ( "(ERROR: Job failed: failed to pull image) \".*(: error during connect)",
        Fun.const None );
      (*       ERROR: Job failed: failed to pull image "[MASKED]/gitlab-runner-helper:x86_64-v14.10.1" with specified policies [if-not-present]: Cannot connect to the Docker daemon at tcp://10.0.150.34:2376. Is the docker daemon running? (manager.go:203:0s) *)
      ( "(ERROR: Job failed: failed to pull image) \".*(: Cannot connect to \
         the Docker daemon)",
        Fun.const None );
      ("(ERROR: Job failed: creating cache volume)", Fun.const None);
      ( "(fatal: unable to access 'https://gitlab.com/tezos/tezos.git/':)",
        Fun.const None );
      ( "(WARNING: Uploading artifacts as \".*\" to coordinator... 500 \
         Internal Server Error)",
        Fun.const None );
      ( "(WARNING: Downloading artifacts from coordinator... failed)",
        Fun.const None );
    ]
  in
  let (specific_patterns, full_retry_command)
        : (string * (string -> string option)) list * string option =
    match split_on_chars [ ' '; ':' ] job.name with
    | "tezt" :: _ ->
        ([ ("\\[.*FAILURE.*\\] \\(.*?\\) (.*)", Fun.const None) ], None)
    | "integration" :: "pytest" :: _
    | "integration" :: "pytest_old_protocols" :: _ ->
        let retry_pytest test =
          Some (Util.sf "poetry run pytest tests_python/%s" test)
        in
        ( [
            (* [gw1] [ 84%] FAILED tests_alpha/test_fa12.py::TestFA12Basic::test_get_total_supply_callback[fa12_reference] *)
            ("\\[.*FAILED[^ ]* (.*) ", retry_pytest);
            (* tests_alpha/test_tenderbake.py::TestProtoTenderbake::test_level FAILED   [100%] *)
            ("(.*) .*FAILED.* \\[", retry_pytest);
            ("(.*) .*ERROR.* \\[", retry_pytest);
          ],
          Some "make test-python" )
    | [ "unit"; "js_components" ] ->
        ( [
            ( "(npm ERR! notarget No matching version found for \
               @nomadic-labs/secp256k1-wasm@0.3.0.)",
              Fun.const None );
          ],
          None )
    | "unit" :: _ ->
        ( [
            (* > [FAIL]        cache               20   load_cache fails if builder fails (`... *)
            ("> \\[FAIL\\] +(.*)", Fun.const None);
            (* test_diff.bc.js: internal error, uncaught exception: *)
            ("(.*: internal error, uncaught exception:)", Fun.const None);
            ("(.* alias .* \\(got signal .*\\))", Fun.const None);
          ],
          None )
    | [ "misc_opam_checks" ] ->
        ( [
            ( "Failed! (The variables `opam_repository_tag` and \
               `full_opam_repository_tag` are not synchronized.)",
              Fun.const None );
          ],
          None )
    (* TODO:

       add a general retry mechanism, and a specific one
    *)
    | [
     ( "install_bin_bionic" | "install_bin_focal" | "install_bin_fedora34"
     | "install_bin_rc_bionic" | "install_bin_rc_focal"
     | "install_bin_rc_fedora34" | "install_opam_scratch"
     | "install_opam_bionic" | "install_opam_focal"
     | "compile_release_sources_buster" | "compile_master_sources_buster"
     | "install_python_bionic" | "install_python_focal"
     | "install_python_buster" );
    ] ->
        ( [],
          Some
            ("./docs/scripts/test_doc_scripts.sh "
            ^ String.(job.name |> split_on_char '_' |> concat "-")) )
    | [ "documentation"; "linkcheck" ] -> ([], Some "make -C docs linkcheck")
    | [ "docker"; "merge_manifests" ] ->
        ( [
            ( "received unexpected HTTP status: 500 Internal Server Error",
              Fun.const None );
          ],
          None )
    | _ -> ([], None)
  in
  let+ matches_opt = grep_trace (specific_patterns @ general_patterns) in
  match matches_opt with
  | Some failures -> Some (Extra_info { failures; full_retry_command })
  | None -> None
