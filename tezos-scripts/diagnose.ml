open Util

type exn += Invalid_date of string

let per_page = 200
let string_of_datetime = ISO8601.Permissive.string_of_datetime
let show_pipeline_job_status = Gitlab_j.string_of_pipeline_job_status

type job_failure = {
  job : Gitlab_t.pipeline_job;
  reason : Tezos_gitlab.extra_info option;
}

let hashtbl_update tbl ~default k f =
  f (Option.value ~default @@ Hashtbl.find_opt tbl k) |> Hashtbl.replace tbl k

let get_failed_pipeline ~token ~project_id ~pipeline_id ~include_retried () :
    job_failure list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let* failed_jobs =
    return
    @@ Gitlab.Project.pipeline_jobs ~token ~project_id ~per_page ~pipeline_id
         ~scope:`Failed ~include_retried ()
  in
  let* failed_jobs = Stream.to_list failed_jobs in
  monad_list_map_p failed_jobs @@ fun job ->
  let+ reason = Tezos_gitlab.get_extra_info ~token ~project_id job in
  { job; reason }

type pipeline_failure = {
  pipeline : Gitlab_t.pipeline;
  failures : job_failure list;
}

type format = Ascii | Markdown | JSON | CSV

let link_pipeline_id pipeline_id =
  "https://gitlab.com/tezos/tezos/-/pipelines/" ^ string_of_int pipeline_id

let format_failure_of_pipeline_ascii fmt ~pipeline_id { job; reason } =
  ignore pipeline_id;
  ff fmt " - job [#%d](%s) (`%s`):" job.id job.web_url job.name;
  match reason with
  | None -> ff fmt " (job trace not found)\n"
  | Some (Tezos_gitlab.Extra_info { failures; full_retry_command }) ->
      ff fmt "\n";
      List.iter
        (fun Tezos_gitlab.{ description; retry_command } ->
          ff fmt "   - %s%s\n" description
            (match retry_command with
            | None -> ""
            | Some retry_command -> sf " (to retry: `%s`)" retry_command))
        failures;
      Option.iter
        (fun full_retry_command ->
          ff fmt "\n   To retry the full job, do: `%s`\n" full_retry_command)
        full_retry_command

let format_failure_of_pipeline_csv ~pipeline_id { job; reason } =
  pf "#%d,%d,%s,%a\n" pipeline_id job.id job.name Tezos_gitlab.pp_extra_info_opt
    reason

let format_failures_of_pipeline ~project_id ~pipeline_id format
    (failures : job_failure list) =
  match format with
  | CSV ->
      pf "pipeline_id,job_id,job_name,reason\n";
      List.iter (format_failure_of_pipeline_csv ~pipeline_id) failures
  | Ascii | _ -> (
      let msg =
        sf "Found %d failures in pipeline [#%d](%s) in project %d:\n\n%a"
          (List.length failures) pipeline_id
          (link_pipeline_id pipeline_id)
          project_id
          (Format.pp_print_list (format_failure_of_pipeline_ascii ~pipeline_id))
          failures
      in
      match failures with
      | [] -> pf "%s" msg
      | first_failure :: _ ->
          pf "%s\n" msg;
          let gitlab_create_issue_link ~title ~description ~labels =
            let uri =
              Uri.(of_string "https://gitlab.com/tezos/tezos/-/issues/new")
            in
            let description =
              description ^ "\n\n" ^ "/label "
              ^ String.concat " " (List.map (fun l -> sf {|~"%s"|} l) labels)
            in
            let uri =
              Uri.add_query_params' uri
                [ ("issue[title]", title); ("issue[description]", description) ]
            in
            uri
          in
          let link =
            let failed_jobs =
              (String.concat ", "
              @@ List.map
                   (fun failure -> sf "`%s`" failure.job.name)
                   (take 3 failures))
              ^ if List.compare_length_with failures 3 > 0 then "..." else ""
            in
            let failure_date =
              Option.fold ~none:"?" ~some:ISO8601.Permissive.string_of_date
                first_failure.job.finished_at
            in
            let title =
              sf "Extended test suite failure: %s (%s -- ...)" failed_jobs
                failure_date
            in
            let description = msg in
            let labels = [ "type::incident"; "CI ⚙" ] in
            gitlab_create_issue_link ~title ~description ~labels
          in
          pf "Create issue: %s\n" (Uri.to_string link))

let () =
  let project_id = ref None in
  let pipeline_id = ref None in
  let token = ref None in
  let include_retried = ref false in
  let format = ref Markdown in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        " <TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        " <PROJECT_ID> Numeric GitLab project id." );
      ( "--include-retried",
        Arg.Set include_retried,
        " Also diagnose retried jobs." );
      ( "--format",
        Arg.String
          (function
          | "md" | "markdown" -> format := Markdown
          | "ascii" -> format := Ascii
          | "json" -> format := JSON
          | "csv" -> format := CSV
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown', 'json', 'csv' or \
                    'ascii'"),
        " Format: either 'markdown', 'md', 'json', 'csv' or 'ascii'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options] <pipeline-id>\nOptions are:"
      Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s ->
      match (int_of_string_opt s, !pipeline_id) with
      | Some i, None -> pipeline_id := Some i
      | None, None ->
          raise
          @@ Invalid_argument (sf "Expected numeric pipeline id, got: %s" s)
      | _, Some _ ->
          raise
          @@ Invalid_argument
               (sf "Don't know what to do with extra arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let format = !format in
  let include_retried = !include_retried in
  match !pipeline_id with
  | None ->
      raise
      @@ Invalid_argument
           (sf "Expected numeric pipeline id as anonymous argument, got none.")
  | Some pipeline_id ->
      Lwt_main.run
      @@ Gitlab.Monad.(
           run
             (get_failed_pipeline ~project_id ~token ~pipeline_id
                ~include_retried ()
             >|= format_failures_of_pipeline ~project_id ~pipeline_id format))
