open Util

type exn += Invalid_date of string

let per_page = 200
let string_of_datetime = ISO8601.Permissive.string_of_datetime

let rec monad_list_iter xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* () = f x in
      monad_list_iter xs f
  | [] -> return ()

let rec monad_list_map_s xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x in
      let* xs' = monad_list_map_s xs f in
      return (x' :: xs')
  | [] -> return []

let rec monad_list_map_p xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x and* xs' = monad_list_map_p xs f in
      return (x' :: xs')
  | [] -> return []

let rec monad_list_concat_map_s xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x in
      let* xs' = monad_list_concat_map_s xs f in
      return (x' @ xs')
  | [] -> return []

let rec monad_list_concat_map_p xs f =
  let open Gitlab.Monad in
  match xs with
  | x :: xs ->
      let* x' = f x and* xs' = monad_list_concat_map_p xs f in
      return (x' @ xs')
  | [] -> return []

(* let rec monad_list_concat_map_chunked ~chunk_size xs f =
 *   let open Gitlab.Monad in
 *   
 *   let rec take k xs = match k with
 *     | 0 -> ([], xs)
 *     | k ->
 *        match xs with
 *        | [] -> failwith "take"
 *        | x :: xs' ->
 *           let (hds, tl) = (take (k - 1) ys) in
 *           (x :: hds, tl)
 *   in
 *   let chunks n xs =
 *     take n xs ::
 * 
 *   match xs with
 *   | x :: xs ->
 *       let* x' = f x in
 *       let* xs' = monad_list_concat_map_s xs f in
 *       return (x' @ xs')
 *   | [] -> return [] *)

let show_pipeline_job_status = Gitlab_j.string_of_pipeline_job_status

(* a job (identified by its name) is 'retried' if it appears several times.
   it is flaky if it appears once with  'success'  and once with 'failed'
*)
(* should return [(job_name, [successes], [failures])] *)
type flake_result = {
  name : string;
  successes : Gitlab_t.pipeline_job list;
  failures : (Gitlab_t.pipeline_job * Tezos_gitlab.extra_info option) list;
}

let hashtbl_update tbl ~default k f =
  f (Option.value ~default @@ Hashtbl.find_opt tbl k) |> Hashtbl.replace tbl k

let get_flaky_of_pipeline ?failure_reason ~token ~project_id ~pipeline_id () :
    flake_result list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let job_status = Hashtbl.create 5 in
  let add_status (job : Gitlab_t.pipeline_job) =
    hashtbl_update ~default:([], []) job_status job.name
      (fun (succeded, failures) ->
        match job.status with
        | `Success -> (job :: succeded, failures)
        | `Failed -> (
            match (failure_reason, job.failure_reason) with
            | None, _ -> (succeded, job :: failures)
            | Some r, Some r' when r = r' -> (succeded, job :: failures)
            | Some _, Some _ -> (succeded, failures)
            | Some _, None ->
                (* I don't think this is possible since the job is failed, but who knows? *)
                assert false)
        | _ -> (succeded, failures))
  in
  let* failed_jobs =
    return
    @@ Gitlab.Project.pipeline_jobs ~token ~project_id ~per_page ~pipeline_id
         ~scope:`Failed ~include_retried:true ()
  in
  let* failed_jobs = Stream.to_list failed_jobs in
  if List.length failed_jobs = 0 then
    let* () =
      pft "Pipeline #%d contains no failures jobs (thus no flaky)\n" pipeline_id
    in
    return []
  else
    let* succeeded_jobs =
      return
      @@ Gitlab.Project.pipeline_jobs ~token ~project_id ~per_page ~pipeline_id
           ~scope:`Success ~include_retried:true ()
    in
    let* succeeded_jobs = Stream.to_list succeeded_jobs in
    let jobs = failed_jobs @ succeeded_jobs in
    List.iter add_status jobs;
    let flaky =
      List.filter_map
        (fun (name, (successes, failures)) ->
          match (successes, failures) with
          (* both are non empty *)
          | _ :: _, _ :: _ ->
              let failures = List.map (fun f -> (f, None)) failures in
              Some { name; successes; failures }
          | _ -> None)
        (List.of_seq (Hashtbl.to_seq job_status))
    in
    let* flaky =
      monad_list_map_p flaky @@ fun { name; successes; failures } ->
      let* failures =
        monad_list_map_p failures (fun (job, _) ->
            let+ extra_info =
              Tezos_gitlab.get_extra_info ~token ~project_id job
            in
            (job, extra_info))
      in
      return { name; successes; failures }
    in
    if List.length flaky = 0 then
      let+ () = pft "Pipeline #%d contains no flaky jobs\n" pipeline_id in
      []
    else
      let* () = pft "Pipeline #%d contains flaky jobs:\n" pipeline_id in
      let* () =
        monad_list_iter flaky @@ fun { name; successes; failures } ->
        let* () = pft " - Job '%s':\n" name in
        let* () =
          monad_list_iter failures (fun (failure, extra_info) ->
              Monad.embed
              @@ Lwt_io.printf "   - Failure: %s [reason: %s]\n"
                   failure.Gitlab_t.web_url
                   (Tezos_gitlab.show_extra_info_opt extra_info))
        in
        let* () =
          monad_list_iter successes (fun successes ->
              pft "   - Success: %s\n" successes.Gitlab_t.web_url)
        in
        return ()
      in
      return flaky

module StringSet = Set.Make (struct
  type t = string

  let compare = String.compare
end)

type retried_results = {
  pipeline : Gitlab_t.pipeline;
  flakes : flake_result list;
}

(* should return [(pipeline_id & url & created_at, [job_name, [successes], [failures]])] *)
let get_retried ?rebased_on ?failure_reason ?updated_after ?updated_before
    ?order_by ?sort ~token ~limit project_id :
    retried_results list Gitlab.Monad.t =
  let open Gitlab in
  let open Monad in
  let* updated_after =
    match (rebased_on, updated_after) with
    | Some _, Some _ ->
        raise
        @@ Invalid_argument
             "cannot give both '--rebased-on' and 'updated_after'"
    | Some sha, None ->
        let* commit = Gitlab.Project.Commit.commit ~token ~project_id ~sha () in
        let commit = Gitlab.Response.value commit in
        let created_at = string_of_datetime commit.commit_created_at in
        let+ () =
          pft "Restricting to pipelines rebased on %s (updated_after: %s)\n" sha
            created_at
        in
        Some created_at
    | None, Some updated_after -> return (Some updated_after)
    | None, None -> return None
  in

  let* pipelines =
    return
    @@ Gitlab.Project.pipelines ~token ~project_id ~per_page ~status:`Success
         ?updated_after ?updated_before ?order_by ?sort ()
  in

  let* pipelines = stream_take limit pipelines in

  let* commits_base_master_range =
    match rebased_on with
    | None -> return StringSet.empty
    | Some base_sha ->
        let ref_name = sf "%s~1..master" base_sha in
        let* commits =
          return
          @@ Gitlab.Project.Commit.commits ~token ~project_id ~ref_name ()
        in
        let commit_limit = 500 in
        let* commits = stream_take (commit_limit + 2) commits in
        let all_but_last xs =
          match List.rev xs with [] -> [] | _ :: xs -> List.rev xs
        in
        let commits = all_but_last commits in
        let n_commits = List.length commits in
        if n_commits = 0 then
          let* () =
            pft "Couldn't find any commits in the range %s, terminating\n"
              ref_name
          in
          exit 1
        else if n_commits = commit_limit + 1 then
          let* () =
            pft
              "Range %s contains more than the limit of %d commits, terminating\n"
              ref_name commit_limit
          in
          exit 1
        else
          let+ () =
            pft "Fetched %d commits in the range %s\n" n_commits ref_name
          in
          StringSet.of_list
            (List.map (fun (c : Gitlab_t.commit) -> c.commit_id) commits)
  in

  monad_list_concat_map_p pipelines @@ fun pipeline ->
  let* rebased =
    match rebased_on with
    | None -> return true
    | Some _ ->
        let* commits =
          return
          @@ Gitlab.Project.Commit.commits ~token ~project_id
               ~ref_name:pipeline.sha ()
        in
        let+ commit_opt =
          stream_limited_find ~limit:20
            (fun (commit : Gitlab_t.commit) ->
              StringSet.mem commit.commit_id commits_base_master_range)
            commits
        in
        Option.is_some commit_opt
  in
  if rebased then
    let* () =
      pft "Pipeline #%d created at %s:\n" pipeline.id
        (string_of_datetime pipeline.created_at)
    in
    let* flakes =
      get_flaky_of_pipeline ?failure_reason ~token ~project_id
        ~pipeline_id:pipeline.id ()
    in
    return [ { pipeline; flakes } ]
  else
    let* () =
      pft "Pipeline #%d created at %s (ignored, not rebased)\n" pipeline.id
        (string_of_datetime pipeline.created_at)
    in
    return []

type format = Ascii | Markdown | JSON

let format_flaky_of_pipeline ?failure_reason ~project_id ~limit format
    (flakes : flake_result list) =
  ignore limit;
  ignore failure_reason;
  ignore project_id;
  ignore format;
  ignore flakes;
  Lwt.return_unit

(* let open Gitlab_t in
 * pf "%d MRs open & active since %a with labels %s:\n\n" (List.length mrs)
 *   (Ptime.pp_rfc3339 ()) since
 *   (String.concat ", " labels);
 * List.iter
 *   (fun mr ->
 *     match format with
 *     | Ascii ->
 *         pf "#%d [updated: %s]: %s\n" mr.merge_request_iid
 *           (Gitlab_json.DateTime.unwrap mr.merge_request_updated_at)
 *           mr.merge_request_title
 *     | Markdown ->
 *         pf " - [!%d](%s): `%s`\n" mr.merge_request_iid
 *           mr.merge_request_web_url
 *           (String.concat "'"
 *              (String.split_on_char '`' mr.merge_request_title)))
 *   mrs *)

let format_retried ?rebased_on ?failure_reason ?updated_after ?updated_before
    ?order_by ?sort ~project_id ~limit format (pipelines : retried_results list)
    =
  ignore format;
  (* group tests by reason *)
  let flakes =
    List.filter
      (function { flakes = _ :: _; _ } -> true | _ -> false)
      pipelines
  in
  let show_order = function
    | `Id -> "id"
    | `Status -> "status"
    | `Ref -> "ref"
    | `Updated_at -> "updated_at"
    | `User_id -> "user_id"
  in
  let filters =
    [ sf "limit=%d" limit ]
    @ (match rebased_on with None -> [] | Some sha -> [ sf "sha=%s" sha ])
    @ (match updated_after with
      | None -> []
      | Some updated_after -> [ sf "updated_after=%s" updated_after ])
    @ (match updated_before with
      | None -> []
      | Some updated_before -> [ sf "updated_before=%s" updated_before ])
    @ (match order_by with
      | None -> []
      | Some order_by -> [ sf "order_by=%s" (show_order order_by) ])
    @ (match sort with
      | None -> []
      | Some sort -> [ sf "sort=%s" (Gitlab_j.string_of_sort sort) ])
    @
    match failure_reason with
    | None -> []
    | Some reason ->
        [
          sf "failure_reason=%s"
            (Gitlab_j.string_of_pipeline_job_failure_reason reason);
        ]
  in
  let open Lwt.Syntax in
  let* () =
    Lwt_io.printf
      "Analyzed %d pipelines in project %d%s, found %d flaky pipelines (%.2f%%)\n"
      (List.length pipelines) project_id
      (match filters with
      | [] -> ""
      | _ -> sf " [%s]" (String.concat ", " filters))
      (List.length flakes)
      (100.0
      *. float_of_int (List.length flakes)
      /. float_of_int (List.length pipelines))
  in
  (* group failures by job_name + extra info *)
  let by_job_reason = Hashtbl.create 5 in
  List.iter
    (fun { flakes; _ } ->
      List.iter
        (fun { name; failures; _ } ->
          List.iter
            (fun (job, extra_info) ->
              hashtbl_update by_job_reason ~default:[] (name, extra_info)
                (fun jobs -> job :: jobs))
            failures)
        flakes)
    flakes;
  let sorted =
    List.sort
      (fun (_, flakes1) (_, flakes2) ->
        Int.compare (List.length flakes2) (List.length flakes1))
      (List.of_seq (Hashtbl.to_seq by_job_reason))
  in
  let* () = Lwt_io.printf "Count,Job name,Extra info,Example\n" in
  Lwt_list.iter_s
    (fun ((job_name, extra_info_opt), failures) ->
      Lwt_io.printf "%d,%s,%s,%s\n" (List.length failures) job_name
        (Format.asprintf "%a" Tezos_gitlab.pp_extra_info_opt extra_info_opt)
        (match failures with
        | failure :: _ -> failure.Gitlab_t.web_url
        | [] -> "!"))
    sorted

let () =
  let limit = ref 20 in
  (* tezos/tezos *)
  let project_id = ref None in
  let pipeline_id = ref None in
  let token = ref None in
  let failure_reason : Gitlab_t.pipeline_job_failure_reason option ref =
    ref None
  in
  let rebased_on = ref None in
  let updated_after = ref None in
  let updated_before = ref None in
  let order_by = ref None in
  let sort = ref None in
  let format = ref Markdown in
  let args_spec =
    [
      ( "--private-token",
        Arg.String (fun t -> token := Some (Gitlab.Token.of_string t)),
        "<TOKEN> GitLab private token." );
      ( "--project-id",
        Arg.Int (fun i -> project_id := Some i),
        "<PROJECT_ID> Numeric GitLab project id." );
      ( "--pipeline-id",
        Arg.Int (fun i -> pipeline_id := Some i),
        "<ID> Look for flaky jobs in this pipeline only. Useful for debugging."
      );
      ( "--updated-after",
        Arg.String (fun date -> updated_after := Some date),
        "Only pipelines updated after" );
      ( "--updated-before",
        Arg.String (fun date -> updated_before := Some date),
        "Only pipelines updated before" );
      ( "--order-by",
        Arg.String
          (fun order ->
            let order =
              match order with
              | "id" -> `Id
              | "status" -> `Status
              | "ref" -> `Ref
              | "updated_at" -> `Updated_at
              | "user_id" -> `User_id
              | _ -> assert false
            in
            order_by := Some order),
        "Order results by" );
      ( "--sort",
        Arg.String
          (fun sort_dir ->
            let sort_dir =
              match sort_dir with
              | "asc" -> `Asc
              | "desc" -> `Desc
              | _ -> assert false
            in
            sort := Some sort_dir),
        "Order results by" );
      ( "--rebased-on",
        Arg.String (fun sha -> rebased_on := Some sha),
        "<SHA> Only include pipelines whose ref includes the given commit. \
         Assumes a semi-linear workflow, that the argument of is commited on \
         master, and that pipelines run for branches targetting master. It \
         works by fetching the commits in the range of 'SHA..master' (but only \
         up to 500 commits). Then, it excludes any pipeline that has no \
         ancestor (searches up to the last 20 ancestors) in that range." );
      ( "--failure-reason",
        Arg.String
          (fun s ->
            failure_reason :=
              Some
                (match s with
                | "script_failure" -> `Script_failure
                | "runner_system_failure" -> `Runner_system_failure
                | _ ->
                    raise
                      (Invalid_argument
                         "--failure-reason must be either 'script_failure' or \
                          'runner_system_failure'"))),
        "<REASON> Only include jobs failing of this reason. Can be either \
         'script_failure' or 'runner_system_failure'." );
      ( "--limit",
        Arg.Int
          (fun l ->
            if l < 0 then raise (Invalid_argument "--limit must be positive")
            else limit := l),
        sf "<LIMIT> Only search up to this many pipelines (defaults to %d)"
          !limit );
      ( "--format",
        Arg.String
          (function
          | "md" | "markdown" -> format := Markdown
          | "ascii" -> format := Ascii
          | "json" -> format := JSON
          | _ ->
              raise
              @@ Invalid_argument
                   "--format must be either 'md', 'markdown', 'json' or 'ascii'"),
        " Format. Can be either 'markdown', 'md', 'json' or 'ascii'." );
    ]
  in
  let usage_msg =
    Printf.sprintf "Usage: %s [options]\nOptions are:" Sys.argv.(0)
  in
  Arg.parse args_spec
    (fun s -> raise @@ Invalid_argument (sf "No anonymous arguments: %s" s))
    usage_msg;
  let token, project_id =
    parse_config ~default_project_id:!project_id ~default_token:!token
  in
  let rebased_on = !rebased_on in
  let failure_reason = !failure_reason in
  let limit = !limit in
  let format = !format in
  let updated_after = !updated_after in
  let updated_before = !updated_before in
  let order_by = !order_by in
  let sort = !sort in
  Lwt_main.run
  @@ Gitlab.Monad.(
       run
         (match !pipeline_id with
         | None ->
             let* retried =
               get_retried ?rebased_on ?failure_reason ?updated_after
                 ?updated_before ?order_by ?sort ~token ~limit project_id
             in
             embed
             @@ format_retried ?rebased_on ?failure_reason ?updated_after
                  ?updated_before ?order_by ?sort ~project_id ~limit format
                  retried
         | Some pipeline_id ->
             let* retried =
               get_flaky_of_pipeline ?failure_reason ~project_id ~token
                 ~pipeline_id ()
             in
             embed
             @@ format_flaky_of_pipeline ?failure_reason ~project_id ~limit
                  format retried))
